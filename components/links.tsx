import { useRouter } from 'next/router'
import { Menu, Button, ButtonProps, MenuItemProps } from 'semantic-ui-react'

type MenuItemLinkProps = {
  href: string
} & MenuItemProps

export const MenuItemLink = ({ href, ...rest }: MenuItemLinkProps) => {
  const router = useRouter()

  const handleClick = (e: any) => {
    e.preventDefault()
    router.push(href)
  }

  return <Menu.Item onClick={handleClick} {...rest} link />
}

type ButtonLinkProps = {
  href: string
} & ButtonProps

export const ButtonLink = ({ href, ...rest }: ButtonLinkProps) => {
  const router = useRouter()

  const handleClick = (e: any) => {
    e.preventDefault()
    router.push(href)
  }

  return <Button onClick={handleClick} {...rest} />
}
