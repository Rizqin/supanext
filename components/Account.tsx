import { useState, useEffect } from 'react'
import { isEmpty } from 'lodash'
import { Input, Icon, Button } from 'semantic-ui-react'

import { supabase } from '../utils/supabaseClient'
import Avatar from './Avatar'
import styles from '../styles/Account.module.scss'
import { ButtonLink } from './links'

export default function Account({ session }: any) {
  const [loading, setLoading] = useState<boolean>(true)
  const [username, setUsername] = useState<string>('')
  const [website, setWebsite] = useState<string>('')
  const [avatar_url, setAvatarUrl] = useState<string>('')

  useEffect(() => {
    getProfile()
  }, [session])

  async function getProfile() {
    try {
      setLoading(true)
      const user = supabase.auth.user()

      if (user) {
        let { data, error, status } = await supabase
          .from('profiles')
          .select(`username, website, avatar_url`)
          .eq('id', user.id)
          .single()

        if (error && status !== 406) {
          throw error
        }

        if (data) {
          setUsername(data.username)
          setWebsite(data.website)
          setAvatarUrl(data.avatar_url)
        }
      }
    } catch (error) {
      alert(error.message)
    } finally {
      setLoading(false)
    }
  }

  async function updateProfile(avatarUrl: string) {
    try {
      setLoading(true)
      const user = supabase.auth.user()

      if (user && !isEmpty(user)) {
        const updates = {
          id: user.id,
          username,
          website,
          avatar_url: avatarUrl || avatar_url,
          updated_at: new Date(),
        }

        let { error } = await supabase.from('profiles').upsert(updates, {
          returning: 'minimal', // Don't return the value after inserting
        })

        if (error) {
          throw error
        }
      }
    } catch (error) {
      alert(error.message)
    } finally {
      setLoading(false)
    }
  }

  return (
    <div className={styles.accountContainer}>
      <ButtonLink icon="home" href="/" floated="right" />

      <Avatar
        url={avatar_url}
        size={150}
        onUpload={(url) => {
          setAvatarUrl(url)
          updateProfile(url)
        }}
      />

      <Input iconPosition="left" placeholder="Email" fluid>
        <Icon name="at" />
        <input id="email" type="text" value={session.user.email} disabled />
      </Input>

      <Input
        label="Name"
        value={username}
        onChange={(e) => setUsername(e.target.value)}
        fluid
      />

      <Input
        label="http://"
        placeholder="mysite.com"
        value={website}
        onChange={(e) => setWebsite(e.target.value)}
        fluid
      />

      <Button onClick={() => updateProfile('')} loading={loading} primary>
        Update
      </Button>
    </div>
  )
}
