import { isEmpty } from 'lodash'
import { useContext } from 'react'
import { Header } from 'semantic-ui-react'
import Account from '../components/Account'

import { SessionContext } from '../lib/sessionContext'

const Profile = () => {
  const session = useContext(SessionContext)

  if (isEmpty(session)) {
    return <Header as="h2">Logged out</Header>
  }

  return <Account session={session} />
}

export default Profile
