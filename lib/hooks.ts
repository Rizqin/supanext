import { useState, useEffect } from 'react'
import { supabase } from '../utils/supabaseClient'

export const useSession = () => {
  const [session, setSession] = useState<any>(null)

  useEffect(() => {
    setSession(supabase.auth.session())

    supabase.auth.onAuthStateChange((_event, session) => {
      setSession(session)
    })
  }, [])

  return session
}
